package com.example;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tommi on 02/05/17.
 */
public interface BookRepository extends JpaRepository<Book, Long> {
}
