package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by tommi on 18/04/17.
 */

@Controller
public class BookControllerThyme {

    private List<Book> books = new ArrayList<>();
    private final AtomicLong idCounter = new AtomicLong();

    @PostConstruct
    public void init() {
        books.add(new Book(idCounter.incrementAndGet(), "Koiramäen tarinoita"));
        books.add(new Book(idCounter.incrementAndGet(), "Poliisin poika"));
    }

    @PostConstruct
    public void test() {
        System.out.println("TÄMÄ ON TESTI.");
    }

    @RequestMapping(value = "/thyme/books", method = RequestMethod.GET)
    public String listBooks(Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("bookList", books);

        return "library";
    }

    @RequestMapping(value = "/thyme/books", method = RequestMethod.POST)
    public String addBook(@ModelAttribute Book book) {
        book.setId(idCounter.incrementAndGet());
        books.add(book);

        return "redirect:/thyme/books";
    }

    @RequestMapping(value = "/thyme/books", method = RequestMethod.PUT)
    public void updateBook(@RequestParam Long id, @RequestParam String bookName) {
        //Muokkaa tämä
    }

    @RequestMapping(value = "/thyme/books", method = RequestMethod.DELETE)
    public void deleteBook(@RequestParam Long id) {
        //Muokkaa tämä
    }



}
