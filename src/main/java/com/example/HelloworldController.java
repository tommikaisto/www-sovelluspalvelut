package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tommi on 12/04/17.
 */

@RestController
public class HelloworldController {

    @RequestMapping("/")
    public String index(){
        return "Hello world!";
    }

}
