package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by tommi on 18/04/17.
 */

@RestController
public class BookController {

    private Map<Long, String> books = new HashMap<>();
    private final AtomicLong idCounter = new AtomicLong();

    @PostConstruct
    public void init() { books.put(idCounter.incrementAndGet(), "7 veljestä");}

    @PostConstruct
    public void test() {
        System.out.println("TÄMÄ ON TESTI.");
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public Map<Long, String> listBooks() {
        return books;
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    public void addBook(@RequestParam String bookName) {
        books.put(idCounter.incrementAndGet(), bookName);
    }

    @RequestMapping(value = "/books", method = RequestMethod.PUT)
    public void updateBook(@RequestParam Long id, @RequestParam String bookName) {
        books.put(id, bookName);
    }

    @RequestMapping(value = "/books", method = RequestMethod.DELETE)
    public void deleteBook(@RequestParam Long id) {
        books.remove(id);
    }



}
