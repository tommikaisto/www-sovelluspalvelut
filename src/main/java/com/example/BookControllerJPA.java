package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by tommi on 18/04/17.
 */

@Controller
public class BookControllerJPA {

    private List<Book> books = new ArrayList<>();
    private final AtomicLong idCounter = new AtomicLong();

    @Autowired
    private BookRepository bookRepository;

    @RequestMapping(value = "/jpa/books", method = RequestMethod.GET)
    public String listBooks(Model model) {
        model.addAttribute("book", new Book());
        books = bookRepository.findAll();
        model.addAttribute("bookList", books);

        return "library";
    }

    @RequestMapping(value = "/jpa/book/{id}", method = RequestMethod.GET)
    public String showBook(@PathVariable Long id, Model model){
        Book book = bookRepository.findOne(id);
        model.addAttribute("book", book);
        return "book";
    }

    @RequestMapping(value = "/jpa/books", method = RequestMethod.POST)
    public String addBook(@ModelAttribute Book book) {
//        book.setId(idCounter.incrementAndGet());
//        books.add(book);
        bookRepository.save(book);

        return "redirect:/jpa/books";
    }

    @RequestMapping(value = "/jpa/book/", method = RequestMethod.POST)
    public String updateBook(@ModelAttribute Book book) {
        bookRepository.save(book);
        return "redirect:/jpa/book/" + book.getId();
    }

    @RequestMapping(value = "/jpa/books/delete/{id}", method = RequestMethod.GET)
    public String deleteBook(@PathVariable Long id) {
        bookRepository.delete(id);
        return "redirect:/jpa/books";
    }

}
